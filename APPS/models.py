from django.db import models
from django.utils import timezone
from phonenumber_field.modelfields import PhoneNumberField
import uuid

class User(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    firstname = models.CharField(max_length=50)
    lastname = models.CharField(max_length=50)
    date_added = models.DateTimeField(default=timezone.now) 
    date_updated = models.DateTimeField(default=timezone.now) 
    phone_number = PhoneNumberField(null=False, blank=False, unique=True)
    address = models.TextField() 
    pin = models.CharField(max_length=50)
    balance = models.FloatField(default=0)

    class Meta:
        db_table = 'user'
        

class Topup(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    amount = models.FloatField(default=0)
    balance_before = models.FloatField(default=0)
    balance_after = models.FloatField(default=0)
    date_added = models.DateTimeField(default=timezone.now)
    remarks = models.CharField(max_length=50) 
    status = models.CharField(max_length=50)
    transaction_type = models.CharField(max_length=50)
    
    class Meta:
        db_table = 'topup'
        
class Payment(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    amount = models.FloatField(default=0)
    balance_before = models.FloatField(default=0)
    balance_after = models.FloatField(default=0)
    date_added = models.DateTimeField(default=timezone.now) 
    remarks = models.CharField(max_length=50)
    status = models.CharField(max_length=50)
    transaction_type = models.CharField(max_length=50)

    class Meta:
        db_table = 'payment'
        
class Transfer(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    amount = models.FloatField(default=0)
    balance_before = models.FloatField(default=0)
    balance_after = models.FloatField(default=0)
    date_added = models.DateTimeField(default=timezone.now) 
    remarks = models.CharField(max_length=50)
    status = models.CharField(max_length=50)
    transaction_type = models.CharField(max_length=50)

    class Meta:
        db_table = 'transfer'
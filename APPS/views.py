from django.http import JsonResponse
from rest_framework.views import APIView
from .models import User, Topup, Payment, Transfer

import jwt

# Soal No. 3
class TopupView(APIView):
    def post(self, request):
        try:
            amount = request.POST.get('amount')
            
            token = request.headers.get('Authorization')
            bearer, _, token = token.partition(' ')
            
            # decode token
            payload = jwt.decode(token, 'access_secret', algorithms='HS256')
            
            # checking data (ada atau tidak untuk ganti login authenticated)
            balance = User.objects.filter(id = payload["user_id"]).values()
            
            if len(balance) != 0: 
                # prosessing topup
                balance_before = balance[0]['balance']
                balance_after = balance_before + float(amount)    
                update_user = User.objects.filter(id = payload["user_id"]).update(balance= balance_after)
                            
                user_login =  User.objects.filter(id = payload["user_id"]).first()

                save_topup = Topup(user_id = user_login,  amount = float(amount), balance_before = balance_before, balance_after = balance_after, status = "SUCCESS", transaction_type = "CREDIT")
                save_topup.save()
                
                result =  {
                        "status": "SUCCESS",
                        "result": {
                            "top_up_id"     : save_topup.id,
                            "amount_top_up" : amount,
                            "balance_before": balance_before,
                            "balance_after" : balance_after,
                            "created_date"  : save_topup.date_added
                        }
                    }
            else:
                result = {
                    "message":"Unauthenticated"
                }
                    
            
        except:
            result = {
                "message":"An error occured"
            }
            
        return JsonResponse(result)                
        
class PaymentView(APIView):
    def post(self, request):
        try:
            amount = request.POST.get('amount')
            remarks = request.POST.get('remarks')
            
            token = request.headers.get('Authorization')
            bearer, _, token = token.partition(' ')
            
            # decode token
            payload = jwt.decode(token, 'access_secret', algorithms='HS256')
            
            # checking login
            balance = User.objects.filter(id = payload["user_id"]).values()
            
            # cond 1, checking login
            if len(balance) != 0:              
                balance_before = balance[0]['balance']
                
                # cond 2, checking balance
                if float(balance_before) - float(amount) > 0:
                    # prosessing payment
                    balance_after = balance_before - float(amount)    
                    update_user = User.objects.filter(id = payload["user_id"]).update(balance= balance_after)
                    
                    user_login =  User.objects.filter(id = payload["user_id"]).first()
                    save_payment = Payment(user_id = user_login, amount = float(amount), remarks = remarks, balance_before = balance_before, balance_after = balance_after, status = "SUCCESS", transaction_type = "DEBIT")
                    
                    save_payment.save()
                    
                    result =  {
                            "status": "SUCCESS",
                            "result": {
                                "payment_id"    : save_payment.id,
                                "amount"        : amount,
                                "remarks"       : remarks,
                                "balance_before": balance_before,
                                "balance_after" : balance_after,
                                "created_date"  : save_payment.date_added
                            }
                        }
                
                else:
                    result = {
                        "message":"Balance is not enough"
                    }
                    
            else:
                result = {
                    "message":"Unauthenticated"
                }

            
        except:
            result = {
                "message":"An error occured"
            }
            
        return JsonResponse(result)                

class TransferView(APIView):
    def post(self, request):
        try:
            amount = request.POST.get('amount')
            remarks = request.POST.get('remarks')
            target_user = request.POST.get('target_user')
            
            token = request.headers.get('Authorization')
            bearer, _, token = token.partition(' ')
            
            # decode token
            payload = jwt.decode(token, 'access_secret', algorithms='HS256')
            
            # checking login
            balance = User.objects.filter(id = payload["user_id"]).values()
            
            # cond 1, check login 
            if len(balance) != 0:   
                balance_before = balance[0]['balance']
                
                # cond 2, check balance
                if float(balance_before) - float(amount) > 0:
                    # prosessing Transfer 
                    balance_after = balance_before - float(amount)  
                    update_user = User.objects.filter(id = payload["user_id"]).update(balance= balance_after)
                    
                    balance_target = User.objects.filter(id = target_user).values()[0]['balance']
                    update_target = User.objects.filter(id = target_user).update(balance= balance_target + float(amount))
                    
                    user_login =  User.objects.filter(id = payload["user_id"]).first()
                    save_transfer = Transfer(user_id = user_login, amount = float(amount), remarks = remarks, balance_before = balance_before, balance_after = balance_after, status = "SUCCESS", transaction_type = "DEBIT")
                    save_transfer.save()
                    
                    result =  {
                            "status": "SUCCESS",
                            "result": {
                                "transfer_id"   : save_transfer.id,
                                "amount"        : amount,
                                "remarks"       : remarks,
                                "balance_before": balance_before,
                                "balance_after" : balance_after,
                                "created_date"  : save_transfer.date_added
                            }
                        }
                    
                else:
                    result = {
                        "message":"Balance is not enough"
                    }
                
            else:
                result = {
                    "message":"Unauthenticated"
                }
                
        except:
            result = {
                "message":"An error occured"
            }
            
        return JsonResponse(result)                

class ReportTransaction(APIView):
    def get(self, request):
        try:
            token = request.headers.get('Authorization')
            bearer, _, token = token.partition(' ')

            payload = jwt.decode(token, 'access_secret', algorithms='HS256')
            
            # checking login
            user_login = User.objects.filter(id = payload["user_id"]).values()
            if len(user_login) != 0:
                all_transaction = []
                
                # report topup transaction
                topup_transaction = Topup.objects.filter(user_id = payload["user_id"])
                
                for i in topup_transaction:
                    topup_trans = {
                        'top_up_id' : i.id,
                        'status' : i.status,
                        'user_id' : payload["user_id"],
                        'transaction_type' : i.transaction_type,
                        'amount' : i.amount,
                        'remarks' : "i.remarks",
                        'balance_before' : i.balance_before,
                        'balance_after' : i.balance_after,
                        'created_date' : i.date_added
                    }
                    
                    all_transaction.append(topup_trans)
                
                # report payment transaction
                payment_transaction = Payment.objects.filter(user_id = payload["user_id"])
                
                for i in payment_transaction:
                    payment_trans = {
                        'payment_id' : i.id,
                        'status' : i.status,
                        'user_id' : payload["user_id"],
                        'transaction_type' : i.transaction_type,
                        'amount' : i.amount,
                        'remarks' : i.remarks,
                        'balance_before' : i.balance_before,
                        'balance_after' : i.balance_after,
                        'created_date' : i.date_added
                    }
                    
                    all_transaction.append(topup_trans)
                
                # report transfer transaction
                transfer_transaction = Transfer.objects.filter(user_id = payload["user_id"])
                
                
                for i in transfer_transaction:
                    transfer_trans = {
                        'transfer_id' : i.id,
                        'status' : i.status,
                        'user_id' : payload["user_id"],
                        'transaction_type' : i.transaction_type,
                        'amount' : i.amount,
                        'remarks' : i.remarks,
                        'balance_before' : i.balance_before,
                        'balance_after' : i.balance_after,
                        'created_date' : i.date_added
                    }
                    
                    all_transaction.append(transfer_trans)
                
                # sorting transaction
                all_transaction = sorted(all_transaction, key = lambda i: i['created_date'], reverse = True)
                
                result = {
                    "status":"SUCCESS",
                    "result":all_transaction
                }
                
            else:
                result = {
                    "message":"Unauthenticated"
                }
                
        except:
            result = {
                "message":"An error occured"
            }
        
        return JsonResponse(result)
from django.urls import path
from .views import *
from .authentication import *

urlpatterns = [
    # authentication
    path('register', Register.as_view()),
    path('login', LoginView.as_view()),
    path('profile', ProfileView.as_view()),
    
    # transaction
    path('topup', TopupView.as_view()),
    path('pay', PaymentView.as_view()),
    path('transfer', TransferView.as_view()),
    
    # report
    path('transactions', ReportTransaction.as_view())
    
    
    
]
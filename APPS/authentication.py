from django.http import JsonResponse
from rest_framework.views import exception_handler
from rest_framework.views import APIView

from .models import User
import jwt, datetime

def status_code_handler(exc, context):
    response = exception_handler(exc, context)

    if response is not None and response.status_code == 403:
        response.status_code = 401

    return response

# soal No. 1
class Register(APIView):
    def post(self, request):
        fname = request.POST.get('first_name')
        lname = request.POST.get('last_name')
        pnumber = request.POST.get('phone_number')
        addr = request.POST.get('address')
        pin_ = request.POST.get('pin')
        
        try:
            cek_phone = User.objects.filter(phone_number = pnumber).values('phone_number')
            
            # checking phone number
            if len(cek_phone) > 0:
                cek_phone = cek_phone[0]['phone_number']

            if pnumber == cek_phone:
                result={
                "message":"Phone Number already registered"
                }   
            else:
                save_regist = User(firstname = fname, lastname = lname, phone_number = pnumber, address = addr, pin = pin_)
                save_regist.save()
                
                result = {
                    "status": "SUCCESS",
                    "result":{
                        "uuid":save_regist.id,
                        "first_name":fname,
                        "last_name":lname,
                        "phone_number":pnumber,
                        "address":addr,
                        "created_date":save_regist.date_added
                    }
                }
                
        except:
            result={
                    "message":"Phone Number already registered"
            }
    
        return JsonResponse(result)        

# soal No. 2
class LoginView(APIView):
    def post(self, request):
        pnumber = request.POST.get('phone_number')
        pin_ = request.POST.get('pin')
        
        # check login
        check_log = User.objects.filter(phone_number = pnumber, pin=pin_).values('phone_number', 'pin')
        if len(check_log) != 0:
            
            if check_log[0]['phone_number'] == pnumber and check_log[0]['pin'] == pin_:
                user = User.objects.filter(phone_number=pnumber).first()
                        
                # encode untuk build token
                access_token = jwt.encode({
                                    'user_id': str(user.id),
                                    'exp': datetime.datetime.utcnow() + datetime.timedelta(hours=23),
                                    'iat': datetime.datetime.utcnow()
                                }, 'access_secret', algorithm='HS256')
                
                
                refresh_token = jwt.encode({
                                    'user_id': str(user.id),
                                    'exp': datetime.datetime.utcnow() + datetime.timedelta(days=7),
                                    'iat': datetime.datetime.utcnow()
                                }, 'refresh_secret', algorithm='HS256')

                result = {
                    "status": "SUCCESS",
                    "result":{
                        "access_token":access_token,
                        "refresh_token":refresh_token
                    }
                }
                
        else:
            result =  {
                "message": "Phone number and pin doesn't match."
            }
            
        return JsonResponse(result)                

# soal no 7
# update data
class ProfileView(APIView):
    def put(self, request):
        first_name = request.GET.get('first_name')
        last_name = request.GET.get('last_name')
        address = request.GET.get('address')
        
        try:
            token = request.headers.get('Authorization')
            bearer, _, token = token.partition(' ')

            payload = jwt.decode(token, 'access_secret', algorithms='HS256')
            user_id = payload["user_id"]
            
            # check user login
            user_login = User.objects.filter(id = user_id).values()
            
            if len(user_login) != 0:
                
                update_user = User.objects.filter(id = user_id).update(firstname=first_name, lastname=last_name, address=address)
            
                result = {
                    "status":"SUCCESS",
                    "result":{
                        "user_id":user_id,
                        "first_name":first_name,
                        "last_name":last_name,
                        "address":address,
                        "update_date":User.objects.filter(id = user_id).values('date_updated')[0]['date_updated']
                    }
                }
                
            else:
                result = {
                    "message":"Unauthenticated"
                }
                
        except:
            result = {
                "message":"An error occured"
            }

        return JsonResponse(result)